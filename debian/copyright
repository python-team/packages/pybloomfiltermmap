Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pybloomfiltermmap
Source: https://pypi.python.org/pypi/pybloomfiltermmap/

Files: *
Copyright: 2013 Michael Axiak <mike@axiak.net>
           2013 Rob Stacey <hello@robstacey.com>
License: MIT

Files: debian/*
Copyright: 2013 Luciano Bello <luciano@debian.org>
License: MIT

Files: src/md5.*
Copyright: 2002 Aladdin Enterprises
License: Zlib

Files: src/superfast.h
Copyright: 2004-2008 Paul Hsieh <qed@pobox.com>
License: LGPL-2.1

License: LGPL-2.1
 The complete text of the GNU Lesser General Public License Version can be
 found in `/usr/share/common-licenses/LGPL-2.1'.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
